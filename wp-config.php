<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bird_international');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yJ?#i=v35:gea2enLPhS?^O$:Z3;tu&~AaQx2HVt56YY|{)z!QV]pNbkArknH<an');
define('SECURE_AUTH_KEY',  'cVd7[?Za-cMVwI/P9]c5^}92.`S|=|f|u>-0K6}h |Ze9`=B~bpe`(5jSo9;1=bs');
define('LOGGED_IN_KEY',    'wj9Fh~tw4J^H_UFB(7+ )UDyDQ/Uvrsos3*>fV@hn15IFccllP3#,y<o_~t(99Ug');
define('NONCE_KEY',        '{c|WW/[#^;f@[L}6h]%h0J:5Ju+AyETt&U@n]((J`Df T_0Dbz1yz|GOh<6qt?b;');
define('AUTH_SALT',        '/6#zNi<uCg!*<*T;:H6cy{<{%E>u?=VWdIo4KCcp&M->xU4dC^:C@_#liUXNZe}t');
define('SECURE_AUTH_SALT', ')0HOzPTHg+1QNl}8qgZp?R< <grtz]T2<]c%hLp?fEVC6M%@&@Pw7#!c(*@W^k|_');
define('LOGGED_IN_SALT',   'W)(cw%@i*V|N@nCX0~0g:K0IS6ZiCSx1oVt{$.#n]ChJd+ |H<YcJ!<%L$#bHBsO');
define('NONCE_SALT',       'MSye:abv%N>wHIr]ze1 178A?ilKV@E>heaoV7}gY3%4,K81uLr5}sW9u.6)3`%7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
