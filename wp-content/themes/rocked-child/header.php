<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Rocked
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<div class="container">
<!-- 	<div class="row">
		<div class="site-branding col-md-6 col-sm-12 col-xs-12">
			<a href="">
				<img src="<?= get_stylesheet_directory_uri() .'/img/logo.png'?>" alt="">
			</a>
		</div>		

		<div class="col-md-6 col-sm-12 col-xs-12 text-center">			
			<div class="row ">				
				<div class="col-md-12 ">
					<div class="social-logo">
						<img src="<?= get_stylesheet_directory_uri() .'/img/fb.png'?>" alt="">
						<img src="<?= get_stylesheet_directory_uri() .'/img/tw.png'?>" alt="">
						<img src="<?= get_stylesheet_directory_uri() .'/img/gplus.png'?>" alt="">
						<img src="<?= get_stylesheet_directory_uri() .'/img/link.png'?>" alt="">
					
						
						
					</div>					
				</div>
			</div>	
			<div class="row">
				<div class="col-md-12">
					<b>email@gmail.com | (555) 555 5555</b>
				</div>
			</div>		
		</div>
	</div> -->

	<?php if ( is_active_sidebar( 'page-header-left' ) ) : ?>
		<div class="site-branding col-md-6 col-sm-12 col-xs-12">
			<?php dynamic_sidebar( 'page-header-left' ); ?>
		</div>
	<?php endif; ?>
	<div class="site-branding col-md-6 col-sm-12 col-xs-12 ">
		<div class="row">			

			<?php if ( is_active_sidebar( 'page-header-right' ) ) : ?>

				<?php dynamic_sidebar( 'page-header-right' ); ?>

			<?php endif; ?>

		</div>

				<div class="row">			

			<?php if ( is_active_sidebar( 'page-header-contact' ) ) : ?>

				<?php dynamic_sidebar( 'page-header-contact' ); ?>

			<?php endif; ?>

		</div>

	</div>

</div>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="preloader">
    <div class="preloader-inner">
    	<?php $preloader = get_theme_mod('preloader_text', __('Loading&hellip;','rocked')); ?>
    	<?php echo esc_html($preloader); ?>
    </div>
</div>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'rocked' ); ?></a>

	<header id="header" class="header">
		<div class="header-wrap">
			<div class="container-fluid">
				<div class="row">
					<!-- <div class="site-branding col-md-3 col-sm-3 col-xs-3"> -->
						<?php //rocked_branding(); ?>
					<!--</div> /.col-md-2 -->
					<div class="menu-wrapper col-md-12 col-sm-12 col-xs-12">
						<div class="btn-menu"><i class="fa fa-bars"></i></div>
						<nav id="mainnav" class="mainnav">
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
						</nav><!-- #site-navigation -->
					</div><!-- /.col-md-10 -->
				</div><!-- /.row -->
			 </div><!--/container -->
		</div>
	</header>
<?php 

	if (is_front_page()) {
		 echo do_shortcode("[metaslider id=53]");
	}
    
?>	
	<?php if ( get_header_image() && ( get_theme_mod('front_header_type' ,'image') == 'image' && is_front_page() || get_theme_mod('site_header_type', 'image') == 'image' && !is_front_page() ) ) : ?>
	<div class="header-image parallax">
		<?php rocked_header_text(); ?>		
	</div>
	<?php endif; ?>

	<div class="main-content " id = "bird-background-image">
		<div class="container">
			<div class="row">