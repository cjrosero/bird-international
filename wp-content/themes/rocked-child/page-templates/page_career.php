<?php

/*

Template Name: Career

*/
 get_header(); ?>

<h2><?php the_title(); ?></h2>

<div class="col-md-8">
	

<?php 
  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

  $custom_args = array(
      'post_type' => 'post',
      'category_name' => 'careers',
      'posts_per_page' => 10,
      'paged' => $paged
    );

  $custom_query = new WP_Query( $custom_args ); ?>

  <?php if ( $custom_query->have_posts() ) : ?>

    <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
      <div class="row">     
			
			<div class="post-container">
				<div class="col-md-2">
					
					<?php if ( has_post_thumbnail() && ( get_theme_mod( 'index_feat_image' ) != 1 ) ) : ?>
						<div class="thumbnail">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('rocked-large-thumb'); ?></a>
						</div>
					<?php endif; ?>

				</div>
				<div class="col-md-10">
					  <a href="<?= the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
						 <?php the_excerpt(); ?>
				</div>
			</div>

       
      </div>
    <?php endwhile; ?>
    <!-- end of the loop -->

    <!-- pagination here -->
    <?php
      if (function_exists(custom_pagination)) {
        custom_pagination($custom_query->max_num_pages,"",$paged);
      }
    ?>

  <?php wp_reset_postdata(); ?>
  <?php else:  ?>
    <p><?php _e( 'Sorry, no posts available' ); ?></p>
  <?php endif; ?>

</div>

<div class="col-md-4">

<div style = "background-color: red;padding:10px;">
				<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
				
					<?php dynamic_sidebar( 'sidebar-1'); ?>
			
			<?php endif; ?>	
</div>


</div>

<?php get_footer(); ?>