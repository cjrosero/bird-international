<?php
/**
 *
 * @package Rocked
 */
?>

	<?php //Set widget areas classes based on user choice
		$widget_areas = get_theme_mod('footer_widget_areas', '3');
		if ($widget_areas == '3') {
			$cols = 'col-md-4';
		} elseif ($widget_areas == '2') {
			$cols = 'col-md-6';
		} else {
			$cols = 'col-md-12';
		}
	?>

	<div id="new-footer" class="footer-widgets footer" role="complementary">
			<div class="container-fluid">
			<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
				<div class="col-md-4">
					<?php dynamic_sidebar( 'footer-1'); ?>
				</div>
					<?php endif; ?>	
				<div class="col-md-8">

					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>				
					<div class="row">
			
					<?php dynamic_sidebar( 'footer-2'); ?>								
					</div>
					<?php endif; ?>	
					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
					<div class="row">
							
					<?php dynamic_sidebar( 'footer-3'); ?>
					</div>
				<?php endif; ?>
				</div>
		</div>	
	</div>



 <!-- <div class = "row">
		<div class="col-md-4 footer-panel">
			<div class="col-md-2">
				<div class="footer-image-left">
					<img src="<?php echo get_stylesheet_directory_uri() .'/img/map-pin.fw.png'?>" alt="">					
				</div>
			</div>
			<div class="col-md-10">
				<div class="row">
					<span class = "footer-color-header" >Our Location</span>	</br>			
					<span class = "footer-color-lower">Manila Philippines</span>
				</div>
			</div>			
		</div>

		<div class="col-md-4 footer-panel">
			<div class="col-md-2">
				<div class="footer-image-left">
					<img src="<?php echo get_stylesheet_directory_uri() .'/img/phone.png'?>" alt="">
					
				</div>
			</div>
			<div class="col-md-10">
				<div class="row">
					<span class = "footer-color-header" >Contact Number</span>	</br>			
					<span class = "footer-color-lower">Phone: (123) 456-7890</span>
				</div>
			</div>			
		</div>

		<div class="col-md-4 footer-panel">
			<div class="col-md-2">
				<div class="footer-image-left">
					<img src="<?php echo get_stylesheet_directory_uri() .'/img/email.png'?>" alt="">
					
				</div>
			</div>
			<div class="col-md-10">
				<div class="row">
					<span class = "footer-color-header" >Email Address</span>	</br>			
					<span class = "footer-color-lower">emailaddress@gmail.com</span>
				</div>
			</div>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 footer-panel">
			<div class="col-md-12">
				<div class="row">
					<span class = "footer-touch" >GET IN TOUCH</span>	</br>			
					<div class="social-logo-footer">
						<img src="http://localhost/bii/wp-content/uploads/img/fb.png" alt="">
						<img src="http://localhost/bii/wp-content/uploads/img/fb.png" alt="">
						<img src="http://localhost/bii/wp-content/uploads/img/fb.png" alt="">
						<img src="http://localhost/bii/wp-content/uploads/img/fb.png" alt="">
						<img src="http://localhost/bii/wp-content/uploads/img/fb.png" alt="">
						
					</div>
				</div>
			</div>			
		</div>
	</div> -->
