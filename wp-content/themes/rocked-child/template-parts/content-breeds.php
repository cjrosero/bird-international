<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Rocked
 */
?>


<h1 class="text-center">BREEDS</h1>
<?php 
$args = array( 'parent' => 11,'number' => 4 );
$breed_categories = get_categories($args);
// echo "<pre>";
// var_dump($breed_categories);
// echo "</pre>";


foreach ($breed_categories as $row) :

$name =  $row->name;
$img = templ_get_the_icon(array('size'=> 'thumbnail'),'category',$row->term_id);
$cat_link = get_category_link($row->cat_ID);
$description = $row->description;


if (isset($img[1][0])) {
	$image = $img[1][0];
	$img_path =  $image;
		// if ($image !== "") {

		// }else{
		// 	$img_path = "";
		// }
}else{
	$img_path = "";
}
?>


<div class="col-md-3 col-sm-12 col-xs-12 tooltip">
	<div class="text-center">
<?php if ($description!== ""): ?>
	<span class="tooltiptext"><?=$description?></span>
<?php endif ?>
	  

	<a href="<?=$cat_link ?>">
		<img src="<?=$img_path?>" alt="">
		<h2 class="text-center f-breeds"><?=$name?></h2>
	</a>		
	</div>
</div>



<?php endforeach;?>




