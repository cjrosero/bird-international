<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Rocked
 */

?>

<article class="loop" style = "background-color:red">
        <h3><?php the_title(); ?></h3>
        <div class="content">
          <?php the_excerpt(); ?>
        </div>
      </article>
