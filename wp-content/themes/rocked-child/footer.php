<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Rocked
 */
?>

			</div>
		</div>
	</div>

		<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
					<?php get_sidebar('footer'); ?>
		<?php endif; ?>
		

<div class = "footer-wrapper">
	

		<div class="container-fluid">
			<div class="row">
				<div class = "menu-wrapper col-md-12 col-sm-12 col-xs-12">
			<nav id="mainnav" class="mainnav">
				<?php wp_nav_menu( array( 'theme_location' => 'second' ) ); ?>  
					</nav>
				</div>
			</div>
		</div>
</div>

<!-- 	<div id="new-footer" >
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-4">
					
				</div>
				<div class="col-md-8 col-sm-8 col-xs-8">
					
				</div>
			</div>
		</div>
	</div> -->
</div><!-- #page -->
<a class="go-top">
	<i class="fa fa-angle-up"></i>
</a>

<?php wp_footer(); ?>

</body>
</html>
