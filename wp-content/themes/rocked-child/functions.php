<?php 
function child_rock_widget_init_left(){
	register_sidebar( array(
		'id'          => 'page-header-left',
		'name'        => __( 'Header','x'),
		'description' => 'This is the Header section of the page.',
		'before_widget' => '<aside>',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	) );
}

add_action('widgets_init','child_rock_widget_init_left');

function child_rock_widget_init_right(){
	register_sidebar( array(
		'id'          => 'page-header-right',
		'name'        => __( 'Header right','x'),
		'description' => 'This is the Header section of the page.',
		'before_widget' => '<div class = "col-md-offset-3 ">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	) );
}

add_action('widgets_init','child_rock_widget_init_right');

function child_rock_widget_init_contact(){
	register_sidebar( array(
		'id'          => 'page-header-contact',
		'name'        => __( 'Contact Info','x'),
		'description' => 'This is the Header section of the page.',
		'before_widget' => '<div class = "text-center">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	) );
}

add_action('widgets_init','child_rock_widget_init_contact');


function child_rock_widget_init_getintouch(){
	register_sidebar( array(
		'id'          => 'page-footer-getintouch',
		'name'        => __( 'Footer Get in Touch','x'),
		'description' => 'This is the Header section of the page.',
		'before_widget' => '<div class = "text-center">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	) );
}

add_action('widgets_init','child_rock_widget_init_getintouch');

function php_execute($html){
if(strpos($html,"<"."?php")!==false){ ob_start(); eval("?".">".$html);
$html=ob_get_contents();
ob_end_clean();
}
return $html;
}
add_filter('widget_text','php_execute',100);



register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'rocked' ),
		'second' => esc_html__( 'Secondary Menu', 'rocked' ),
	) );

function wpb_postsbycategory() {
// the query	
	$the_query = new WP_Query( array( 'category_name' => 'careers', 'posts_per_page' => 5) ); 


// The Loop
	if ( $the_query->have_posts() ) {
		$string .= '<ul class="postsbycategory widget_recent_entries">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			if ( has_post_thumbnail() ) {
				$string .= '<li>';
				$string .= '<a href="' . get_the_permalink() .'" rel="bookmark">' . get_the_post_thumbnail($post_id, array( 50, 50) ) . get_the_title() .'asd</a>';
				$string .= '<p>';
				$string .= get_the_content()."asd";
				$string .= '</p></li>';
			} else { 
			// if no featured image is found

				
				$string .= '<li><a href="' . get_the_permalink() .'" rel="bookmark">' . get_the_title() .'</a>';
				$string .= '<p>';
				$string .= (strlen(get_the_content()) > 20) ? substr(get_the_content(),0,20) . " . . . " :  get_the_content();
				$string .= '</p></li>';
			}
		}
				
	} else {
	// no posts found
	}
	$string .= '</ul>';
	

	return $string;

	/* Restore original Post Data */
	wp_reset_postdata();
}
// Add a shortcode
add_shortcode('categoryposts', 'wpb_postsbycategory');


function bi_pagination_settings(){
	global $wp_query;
$total = $wp_query->max_num_pages;
// only bother with the rest if we have more than 1 page!
if ( $total > 1 )  {
     // get the current page
     if ( !$current_page = get_query_var('paged') )
          $current_page = 1;
     // structure of "format" depends on whether we're using pretty permalinks
     if( get_option('permalink_structure') ) {
	     $format = '&paged=%#%';
     } else {
	     $format = 'page/%#%/';
     }
     echo paginate_links(array(
          'base'     => get_pagenum_link(1) . '%_%',
          'format'   => $format,
          'current'  => $current_page,
          'total'    => $total,
          'mid_size' => 4,
          'type'     => 'list'
     ));
}

}

add_shortcode('career_pagination', 'bi_pagination_settings');
// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');


function bi_custom_query(){
	$string = "";
		$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
		$query_args = array(
			'post_type' => 'post',
			'category_name' => 'careers',
			'posts_per_page' => 2,
			'paged' => $paged
		);

		$the_query = new WP_Query( $query_args );

		if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
			
			$string .='<article>';
			$string .= '<h1>'.the_title().'</h1>';
			$string .= '<div class="excerpt">'. the_excerpt(); 
			$string .= '  </div>';
			$string .= ' </article>';

			endwhile;

			if ($the_query->max_num_pages > 1):
			$string .= '<nav class="prev-next-posts">';
				$string .= '<div class="prev-posts-link">'. get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); 
				$string .= '</div>';
				$string .= '<div class="next-posts-link">'. get_previous_posts_link( 'Newer Entries' );
				$string .= '</div>';
			$string .= '</nav>';
				endif;
		endif;

		return $string;
  
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}


add_shortcode('career_query', 'bi_custom_query');

function get_category_breeds(){
	return get_template_part( 'template-parts/content', 'breeds' );
}

add_shortcode('categorybreeds', 'get_category_breeds');
