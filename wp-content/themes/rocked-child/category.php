<?php
/**
* A Simple Category Template
*/

get_header(); ?> 

<section id="primary" class="site-content">
	<div id="content" role="main">

		<?php 
// Check if there are any posts to display
		if ( have_posts() ) : ?>

		<header class="archive-header">
			<h1 class="archive-title text-center"><?php single_cat_title(); ?></h1>


			<?php
// Display optional category description
			if ( category_description() ) : ?>
			<div class="cat_desc text-center"><?php echo category_description(); ?></div>
		<?php endif; ?>
	</header>

	<?php

// The Loop
	while ( have_posts() ) : the_post(); ?>
<!-- 	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	<small><?php the_time('F jS, Y') ?> by <?php the_author_posts_link() ?></small>

	<div class="entry">
		<?php the_content(); ?>

		<p class="postmetadata"><?php
			comments_popup_link( 'No comments yet', '1 comment', '% comments', 'comments-link', 'Comments closed');
			?></p>
		</div> -->
		
		
		<div class="col-md-4 ">		
			<div class="content-list">
				<div class="row">
					<a href="<?=get_permalink();?>">
						<div class="thumbnail thumb-category">	
							<?php

							 if (has_post_thumbnail()){ ?>
								<?=the_post_thumbnail()?>
								<div class="middle">
									<div class="cat-text"><?=the_title()?></div>
								</div>	

							<?php }else{ ?>
							<?php 
								$category_id = get_the_category()[0]->cat_ID;						
								$category_image =  templ_get_the_icon(array('size'=> 'thumbnail'),'category',$category_id );
								$image_path = $category_image[1][0];?>

								<img src="<?=$image_path;?>" alt="">

							<?php } ?>		

						</div>	
					</a>
				</div>

				<div class="row">
					<a href="<?=get_permalink();?>"><h5><?=the_title();?></h3></a>
					<p>
						<?php
						the_excerpt();
						?>
					</p>
				</div>
			</div>
		</div>	





	<?php endwhile; 

	else: ?>
	<p>Sorry, no posts matched your criteria.</p>


<?php endif; ?>
</div>
</section>



<?php get_footer(); ?>